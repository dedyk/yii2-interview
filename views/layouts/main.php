<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => '@web/favicon.ico']);
AppAsset::register($this);
$urlInfo = Yii::$app->params['apiBaseUrl'].'/auth/info';
$urlLogin = \yii\helpers\Url::to(['site/login'],true);
$js =<<<JS
let token = sessionStorage.getItem('token')

$.ajax({
    url: '{$urlInfo}',
    method:'GET',
    contentType: false,
    processData: false,
    async:false,
    headers: {
        Authorization: "Bearer " + token
    },
    success:function(data, textStatus, jqXHR) 
    {
        $('.user-name-text').each(function(){
            $(this).text(data.data.username)
        })
    },
    error: function(jqXHR, textStatus, errorThrown) {
        window.location = "{$urlLogin}";
    },
})


$('#logout-btn').click(function() {
    sessionStorage.removeItem('token')
     window.location = "{$urlLogin}";
})
JS;

$this->registerJs($js)



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable"><head>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- Begin page -->
<div id="layout-wrapper">

    <?php include 'menu.php'; ?>

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <!-- start page title -->
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                            <h4 class="mb-sm-0"><?= $this->title ?></h4>
                            <div class="page-title-right">
                                <?= isset($this->params['breadcrumbs'])? Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]): '' ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Alert::widget() ?>
                <!-- end page title -->
                <?= $content ?>

            </div>
            <!-- container-fluid -->

        </div>
        <!-- End Page-content -->

        <?php include 'footer.php'; ?>
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\contact $model */

$this->params['breadcrumbs'][] = 'Index';


$this->registerCssFile('//cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css');
$this->registerJsFile('https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js',['depends' => [\yii\web\JqueryAsset::class]]);
$this->registerJsFile('//cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js',['depends' => [\yii\web\JqueryAsset::class]]);

$urlApiContact = Yii::$app->params['apiBaseUrl'].'/contacts';
$urlLogin = \yii\helpers\Url::to(['site/login']);
$jsDatatable =<<<JS


let t = new DataTable('#ajax-datatables', {
    ajax: {
        url: '{$urlApiContact}',
        dataSrc(json){
            return json.data;
        },
        type:'GET',
        beforeSend: function (request) {
            let token = sessionStorage.getItem('token')
            request.setRequestHeader("Authorization", 'Bearer '+ token);
        },
         error: function (e) {
            if(e.status==401){
                window.location = "{$urlLogin}";
                sessionStorage.removeItem('token')
            }
        },
    }, 
    columnDefs: [ //add button update and delete
       {  
           "targets": 1,
            "render": function ( data, type, full, meta ) {
                return '<a href="#" data-id="'+data+'" class="update-contact-btn btn btn-warning px-2 py-1"><i class="ri-pencil-line"></i></a> ' +
                  '<a href="#"  data-id="'+data+'" class="delete-contact-btn btn btn-danger px-2 py-1"><i class="ri-delete-bin-line"></i></a>';
                }
        }
    ],
    order: [[1, 'asc']],
    columns: [
        { data: 'id' },
        { data: 'id' },
        { data: 'name' },
        { data: 'surname' },
        { data: 'birthday' },
        { data: 'phone' },
        { data: 'address' }
    ]
});

//add auto increment to table
  t.on('order.dt search.dt', function () {
        let i = 1;
        t.cells(null, 0, { search: 'applied', order: 'applied' }).every(function (cell) {
            this.data(i++);
        });
    }).draw();
JS;


$jsForm = <<<JS
$(document).on('click','#create-btn',function (e){
    $('input[name="name"]').val('')
    $('input[name="surname"]').val('')
    $('input[name="birthday"]').val('')
    $('input[name="phone"]').val('')
    $('textarea[name="address"]').val('')
    $('#contact-form').attr('method','POST')
    $('#contact-form').attr('action','{$urlApiContact}')
    $('#label-form').html('Create')
});
$(document).on('click','.delete-contact-btn',function (e){
    e.preventDefault()
    if(confirm('are you sure you want to delete this?')==false){
        return;
    }
    
   let data_id = this.getAttribute('data-id')
   let token = sessionStorage.getItem('token')
    //get data by id
    $.ajax(
        {
            url : '{$urlApiContact}/'+data_id,
            type: 'DELETE',
            contentType: false,
            processData: false,
            async:false,
            headers: {
                Authorization: "Bearer " + token
            },
            success:function(data, textStatus, jqXHR) 
            {
                document.getElementById('delete-alert').classList.remove('d-none')
                document.getElementById('delete-alert').classList.add('show')
                $('#ajax-datatables').DataTable().ajax.reload(null, false )

  
            },      
        });

});

$(document).on('click','.update-contact-btn',function (e){
    e.preventDefault()
   let data_id = this.getAttribute('data-id')
   let token = sessionStorage.getItem('token')
    //get data by id
    $.ajax(
        {
            url : '{$urlApiContact}/'+data_id,
            type: 'GET',
            contentType: false,
            processData: false,
            async:false,
            headers: {
                Authorization: "Bearer " + token
            },
            success:function(data, textStatus, jqXHR) 
            {
                  $('input[name="name"]').val(data.name)
                  $('input[name="surname"]').val(data.surname)
                  $('input[name="birthday"]').val(data.birthday)
                  $('input[name="phone"]').val(data.phone)
                  $('textarea[name="address"]').val(data.address)
  
            },      
        });
    $('#showModal').modal('show')
    $('#contact-form').attr('method','PUT')
    $('#contact-form').attr('action','{$urlApiContact}/'+data_id)
    $('#label-form').html('Update')
});

$(document).on('beforeSubmit','#contact-form',function async(e){
    e.preventDefault()
    let formURL = $("#contact-form").attr("action");
    let method =  $("#contact-form").attr("method");
    let formData = (method=='POST')? new FormData(this) : $(this).serialize() ;

    let errors=[]
    let token = sessionStorage.getItem('token')
    console.log(token)
    $.ajax(
        {
            url : formURL,
            method: method,
            type: method,
            data : formData,
            contentType: false,
            processData: false,
            async:false,
            headers: {
                Authorization: 'Bearer ' + token,
            },
         
            success:function(data, textStatus, jqXHR) 
            {
                document.getElementById('success-alert').classList.remove('d-none')
                document.getElementById('success-alert').classList.add('show')
                $('#ajax-datatables').DataTable().ajax.reload(null, false )
                $('#showModal').modal('hide')
                
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                 errors= jqXHR.responseJSON.data;
               
            }
        });
    if(errors.length>0){
        document.getElementById('alert-error').classList.add('show')
        document.getElementById('alert-error').classList.remove('d-none')
        Object.keys(errors).forEach(key => {
            $("#error-list").append('<li><strong>'+ key+'</strong> '+errors[key]+'</li>');
        });
    }
    
    return false

});
JS;
$this->registerJs($jsForm,position: \yii\web\View::POS_END);
$this->registerJs($jsDatatable);

?>
<div class="site-index row">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">Contacts</h5>
                </div>
                <div class="card-body">
                    <!-- Success Alert -->
                    <div class="alert alert-success alert-dismissible fade d-none" role="alert" id="success-alert">
                        <strong> Success! </strong> data saved!
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    <div class="alert alert-info alert-dismissible fade d-none" role="alert" id="delete-alert">
                        <strong> Success! </strong> data deleted!
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    <div class="row g-4 mb-3">
                    <div class="col-sm-auto">
                        <div>
                            <button type="button" class="btn btn-success add-btn" data-bs-toggle="modal" id="create-btn" data-bs-target="#showModal"><i class="ri-add-line align-bottom me-1"></i> Add</button>
                        </div>
                    </div>

                </div>
                    <table id="ajax-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Birthday</th>
                            <th>Phone</th>
                            <th>Address</th>
                        </tr>
                        </thead>

                    </table>
                    <div class="modal fade" id="showModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header bg-light p-3">
                                    <h5 class="modal-title" id="exampleModalLabel"><span id="label-form"></span> Contact</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                <?php
                                $form = ActiveForm::begin([
                                    'id' => 'contact-form',
                                    'action' => Yii::$app->params['apiBaseUrl'] . '/contacts'

                                ]); ?>
                                <div class="modal-body">
                                    <?= $this->render('@app/views/partials/error') ?>

                                    <div class="mb-3">
                                        <label class="form-label">Name</label>
                                        <input type="text" name="name"  class="form-control" placeholder="Enter Name" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Surname</label>
                                        <input type="text" name="surname"  class="form-control" placeholder="Enter Surname" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Birthday</label>
                                        <input type="date" name="birthday"  class="form-control" placeholder="Enter Birthday" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Phone</label>
                                        <input type="text" name="phone"  class="form-control" placeholder="Enter Phone" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">Address</label>
                                        <textarea name="address" class="form-control" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="hstack gap-2 justify-content-end">
                                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-success" id="action-form-btn">Save</button>
                                    </div>
                                </div>
                                <?php ActiveForm::end() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end col-->

    </div><!--end row-->



</div>


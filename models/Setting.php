<?php

namespace app\models;

use Yii;
use \app\models\base\Setting as BaseSetting;

/**
 * This is the model class for table "setting".
 */
class Setting extends BaseSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['value'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['name'], 'unique']
        ]);
    }
	
}

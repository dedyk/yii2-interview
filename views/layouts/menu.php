<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;



?>


<header id="page-topbar">
    <div class="layout-width">
        <div class="navbar-header">
            <div class="d-flex">
                <!-- LOGO -->
                <div class="navbar-brand-box horizontal-logo">
                    <a href="<?= Url::home() ?>" class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="<?= Url::to('@web/') ?>images/logo-dark.png" alt="" height="22">
                        </span>
                        <span class="logo-lg">
                            <img src="<?= Url::to('@web/') ?>images/logo-sm-dark.png" alt="" height="180">
                        </span>
                    </a>

                    <a href="<?= Url::home() ?>" class="logo logo-light">
                        <span class="logo-sm">
                            <img src="<?= Url::to('@web/') ?>images/logo-sm.png" alt="" height="22">
                        </span>
                        <span class="logo-lg">
                            <img src="<?= Url::to('@web/') ?>images/logo-light.png" alt="" height="180">
                        </span>
                    </a>
                </div>

                <button type="button" class="btn btn-sm px-3 fs-16 header-item vertical-menu-btn topnav-hamburger"
                        id="topnav-hamburger-icon">
                    <span class="hamburger-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>


            </div>

            <div class="d-flex align-items-center">

                <div class="dropdown d-md-none topbar-head-dropdown header-item">
                    <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle"
                            id="page-header-search-dropdown" data-bs-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        <i class="bx bx-search fs-22"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                         aria-labelledby="page-header-search-dropdown">
                        <form class="p-3">
                            <div class="form-group m-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search ..."
                                           aria-label="Recipient's username">
                                    <button class="btn btn-primary" type="submit"><i
                                            class="mdi mdi-magnify"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>


                <div class="ms-1 header-item d-none d-sm-flex">
                    <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle"
                            data-toggle="fullscreen">
                        <i class='bx bx-fullscreen fs-22'></i>
                    </button>
                </div>

                <div class="ms-1 header-item d-none d-sm-flex">
                    <button type="button"
                            class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle light-dark-mode">
                        <i class='bx bx-moon fs-22'></i>
                    </button>
                </div>



                <div class="dropdown ms-sm-3 header-item topbar-user">
                    <button type="button" class="btn" id="page-header-user-dropdown" data-bs-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <span class="d-flex align-items-center">
                            <img class="rounded-circle header-profile-user" src="<?= Url::to('@web/') ?>images/users/avatar-1.jpg"
                                 alt="Header Avatar">
                            <span class="text-start ms-xl-2">
                                <span class="d-none d-xl-inline-block ms-1 fw-medium user-name-text"></span>

                            </span>
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <h6 class="dropdown-header">Welcome <span class="user-name-text"></span>!</h6>
                            <a class="dropdown-item" href="#" id="logout-btn"><i
                                    class="mdi mdi-account-circle text-muted fs-16 align-middle me-1"></i> <span
                                    class="align-middle">Logout</span></a>


                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ========== App Menu ========== -->
<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="<?= Url::home() ?>" class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="<?= Url::to('@web/') ?>images/logo-dark.png" alt="" height="22">
                        </span>
            <span class="logo-lg">
                            <img src="<?= Url::to('@web/') ?>images/logo-sm-dark.png" alt="" height="180">
                        </span>
        </a>

        <a href="<?= Url::home() ?>" class="logo logo-light">
                        <span class="logo-sm">
                            <img src="<?= Url::to('@web/') ?>images/logo-sm.png" alt="" height="22">
                        </span>
            <span class="logo-lg">
                            <img src="<?= Url::to('@web/') ?>images/logo-light.png" alt="" height="180">
                        </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover"
                id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">
            <div id="two-column-menu">
            </div>
            <?php
            $items = [
              ['label' => 'Menu', 'url' => '#',            'options'=>['class'=>'menu-title'],
                    'template' => '<span>{label}</span>',
                ],
                ['label' => '<i class="ri-home-2-line"></i> <span>Dashboard</span>', 'url' => ['site/index']],

            ];


            echo Menu::widget([
                'items' =>$items,
                'options' => [
                    'class' => 'navbar-nav',
                    'id'=>'navbar-nav',

                ],
                'encodeLabels' => false,
                'itemOptions'=>['class'=>'nav-item'],
                'linkTemplate' => '<a href="{url}" class="nav-link menu-link"  role="button"
                       aria-expanded="false" aria-controls="sidebarDashboards">{label}</a>',
                'activeCssClass'=>'active',

            ]);

            ?>
            <!-- end Dashboard Menu -->
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
    <div class="sidebar-background"></div>
</div>
<!-- Left Sidebar End -->
<!-- Vertical Overlay-->
<div class="vertical-overlay"></div>
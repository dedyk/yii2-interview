<?php

return [
//    'bsDependencyEnabled' => false, // this will not load Bootstrap CSS and JS for all Krajee extensions

    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'bsVersion' => '5.x', // this will set globally `bsVersion` to Bootstrap 5.x for all Krajee Extensions
    'apiBaseUrl'=>'http://yii2-addressbook.test/api',
    'jwtSecret'=>'veGPoPydJnaK9mVneoxPQq_3Z54WxkvK'

];

<?php
namespace app\components;


use yii\helpers\Html;

class ActionButton
{
    public static function getButtons($template = '{update} {delete}',$size='sm',$controller=null,$newButtons=[],$label=null,$id_attribute = 'id',$contentOptions=null)
    {
        return [
            'class' => 'yii\grid\ActionColumn',
            'template' => $template,
            'header'=>$label,
            'buttons' =>
                array_merge($newButtons,
                    [
                        'view' => function ($url, $model, $key) use (&$size,&$controller,$id_attribute) {
                            return Html::a("<i class='ri-search-eye-line'></i>", [is_null($controller)?'view':$controller.'/view', "id" => $model->{$id_attribute}], ["class" => ($size=="sm")? "btn btn-success  px-2 py-1":"btn btn-success", "title" => "Lihat Data"]);
                        },
                        'update' => function ($url, $model, $key) use (&$size,&$controller,$id_attribute) {
                            return Html::a("<i class='ri-pencil-line'></i>", [is_null($controller)?'update':$controller.'/update', "id" => $model->{$id_attribute}], ["class" => ($size=="sm")? "btn btn-warning px-2 py-1":"btn btn-warning", "title" => "Edit Data"]);
                        },
                        'delete' => function ($url, $model, $key) use (&$size,&$controller,$id_attribute) {
                            return Html::a("<i class='ri-delete-bin-line'></i>", [is_null($controller)?'delete':$controller.'/delete', "id" => $model->{$id_attribute}], [
                                "class" => ($size=="sm")? "btn btn-danger  px-2 py-1":"btn btn-danger",
                                "title" => "Hapus Data",
                                "data-confirm" => "Apakah Anda yakin ingin menghapus data ini ?",
                                //"data-method" => "GET"
                                "data-method"=>"post"
                            ]);
                        },
                    ]
                ),
            'contentOptions' => $contentOptions??['nowrap' => 'nowrap', 'style' => 'text-align:center;width:170px']
        ];
    }
}

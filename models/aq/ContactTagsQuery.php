<?php

namespace app\models\aq;

/**
 * This is the ActiveQuery class for [[\app\models\aq\ContactTags]].
 *
 * @see \app\models\aq\ContactTags
 */
class ContactTagsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\aq\ContactTags[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\aq\ContactTags|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

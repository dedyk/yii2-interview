<?php

namespace app\models\aq;

/**
 * This is the ActiveQuery class for [[\app\models\aq\Contact]].
 *
 * @see \app\models\aq\Contact
 */
class ContactQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\aq\Contact[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\aq\Contact|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

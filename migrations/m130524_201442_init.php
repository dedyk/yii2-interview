<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        ini_set("memory_limit", "1G");
        $sql = file_get_contents(Yii::getAlias("@app/db/basic.sql"));
        $command = Yii::$app->db->createCommand($sql);
        $command->execute();
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210724_025850_init_db cannot be reverted.\n";

        return false;
    }

}

<?php

namespace app\models;

use Yii;
use \app\models\base\ContactTags as BaseContactTags;

/**
 * This is the model class for table "contact_tags".
 */
class ContactTags extends BaseContactTags
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['contact_id', 'tag'], 'required'],
            [['contact_id', 'tag'], 'integer']
        ]);
    }
	
}

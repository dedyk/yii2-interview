<?php

namespace app\models\aq;

/**
 * This is the ActiveQuery class for [[\app\models\aq\Tag]].
 *
 * @see \app\models\aq\Tag
 */
class TagQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\aq\Tag[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\aq\Tag|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

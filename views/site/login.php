<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */

/** @var app\models\LoginForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$urlIndex = \yii\helpers\Url::home(true);
$js = <<<JS
$(document).on('beforeSubmit','#login-form',function(e){
    e.preventDefault()
    var formData = new FormData(this);
    var formURL = $("#login-form").attr("action");

    let errors=[]
    $.ajax(
        {
            url : formURL,
            type: "POST",
            data : formData,
           contentType: false,
            processData: false,
            async:false,
            success:function(data, textStatus, jqXHR) 
            {
                sessionStorage.setItem('token',data.data.token)
                window.location = "{$urlIndex}";
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                 errors= jqXHR.responseJSON.data;
               
            },
          
        });
    if(errors.length>0){
        document.getElementById('alert-error').classList.add('show')
        document.getElementById('alert-error').classList.remove('hidden')
        Object.keys(errors).forEach(key => {
            $("#error-list").append('<li><strong>'+ key+'</strong> '+errors[key]+'</li>');
        });
    }
    
    return false

});
JS;
$this->registerJs($js,position: \yii\web\View::POS_END);

?>


<div class="col-lg-6">
    <div class="p-lg-5 p-4">
        <div>
            <?= \app\widgets\Alert::widget() ?>
            <h5 class="text-primary">Welcome</h5>
            <p class="text-muted">Please login to continue!</p>
        </div>

<?= $this->render('@app/views/partials/error') ?>

        <div class="mt-4">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'action' => Yii::$app->params['apiBaseUrl'] . '/auth/login'

            ]); ?>
            <?= $form->field($model, 'username')->textInput(['autofocus' => true])?>

            <?= $form->field($model, 'password')->passwordInput() ?>


            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-success w-100', 'name' => 'login-button']) ?>
            </div>


            <?php ActiveForm::end() ?>
        </div>


    </div>
</div>
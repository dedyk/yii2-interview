<?php

namespace app\controllers\api;

use app\models\User;
use Yii;
use yii\rest\Controller;

class RestController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * Set response format as JSON
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $_GET['_format'] = "json";
        return parent::beforeAction($action);
    }

    /**
     * To handle output each action
     * @param $inputs
     * @param $code
     * @return array
     */
    public function output($inputs, $code = 200)
    {
        \Yii::$app->response->setStatusCode($code);
        return [
            "status" => $code,
            "data" => $inputs
        ];
    }

}
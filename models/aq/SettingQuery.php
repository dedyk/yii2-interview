<?php

namespace app\models\aq;

/**
 * This is the ActiveQuery class for [[\app\models\aq\Setting]].
 *
 * @see \app\models\aq\Setting
 */
class SettingQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\aq\Setting[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\aq\Setting|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

<?php

namespace app\controllers\api;

use app\models\User;
use app\components\JWTSignatureBehavior;
use Dersonsena\JWTTools\JWTTools;
use Yii;
use app\models\LoginForm;
use yii\filters\auth\HttpBearerAuth;

class AuthController extends RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['jwtValidator'] = [
            'class' => JWTSignatureBehavior::class,
            'secretKey' => Yii::$app->params['jwtSecret'],
            'except' => ['login'] // it's doesn't run in login action
        ];

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'except' => ['login'] // it's doesn't run in login action
        ];

        return $behaviors;
    }




    public function actionLogin(){
        $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $token = JWTTools::build(Yii::$app->params['jwtSecret'],
                    options: [
                    '7d'
                ])
                    ->withModel($model->getUser(), ['username', 'email'])
                    ->getJWT();
                return $this->output(['token'=>$token]);
            }else{
                return $this->output($model->errors,400);
            }
    }

    public function actionInfo(){
       return $this->output([
           'username'=>$this->decodedToken->username
       ]);

    }
}
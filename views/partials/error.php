<?php
?>

<div id="alert-error" class="alert alert-danger alert-dismissible alert-solid alert-label-icon fade d-none" role="alert">
    <i class="ri-error-warning-line label-icon"></i><strong>Error</strong> - Please fix this
    <ul id="error-list">
    </ul>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

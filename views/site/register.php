<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="col-lg-6">
    <div class="p-lg-5 p-4">
        <div>
            <h5 class="text-primary">Selamat Datang !</h5>
            <p class="text-muted">Silahkan isi form untuk mendaftar.</p>
        </div>

        <div class="mt-4">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',


            ]); ?>
            <?= $form->errorSummary($model); ?>

                <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

                <?= $form->field($model, 'nama')->textInput(['placeholder' => 'Nama']) ?>

                <?= $form->field($model, 'tipe')->dropDownList(\app\models\EnumTipeLembaga::array()) ?>

                <?= $form->field($model, 'pic')->textInput(['placeholder' => 'Penanggung Jawab']) ?>

                <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'no_telp')->textInput(['placeholder' => 'No Telp']) ?>


            <div class="form-group">
                <?= Html::submitButton('Daftar', ['class' => 'btn btn-success w-100', 'name' => 'login-button']) ?>
            </div>




            <?php ActiveForm::end() ?>
        </div>
        <div class="mt-5 text-center">
            <p class="mb-0">Sudah punya akun ? <a href="<?= \yii\helpers\Url::to(['/site/login']) ?>" class="fw-semibold text-primary text-decoration-underline"> Masuk</a>
            </p>
        </div>


    </div>
</div>
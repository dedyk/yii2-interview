<?php

namespace app\models;

use Yii;
use \app\models\base\Contact as BaseContact;

/**
 * This is the model class for table "contact".
 */
class Contact extends BaseContact
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'surname', 'phone', 'address', 'birthday'], 'required'],
            [['address'], 'string'],
            [['birthday', 'created_at', 'updated_at'], 'safe'],
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'surname'], 'string', 'max' => 250],
            [['phone'], 'string', 'max' => 20],
            [['user_id'], 'default', 'value'=> 1], //set default value on user_id=1
        ]);
    }
	
}
